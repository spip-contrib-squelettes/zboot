<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// C
	'zboot_description' => '',
	'zboot_nom'         => 'Zboot',
	'zboot_slogan'      => 'Squelette de démarrage Zcore + SCSS',
];

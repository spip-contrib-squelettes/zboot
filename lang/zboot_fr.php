<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// 4
	'404_explication'             => 'Cette page n\'existe pas ou a été déplacée.',

	// C
	'zboot_titre'                 => 'Zboot',

	// I
	'info_1_resultat'             => 'Un résultat',
	'info_nb_resultats'           => '@nb@ résultats',

	// N
	'navigation_principale'       => 'Navigation principale',

	// P
	'pagination_suiv' => 'Page suivante',
	'pagination_prec' => 'Page précédente',
	
	// T
	'titre_page_configurer_zboot' => 'Configuration',

	// V
	'vous_etes_ici'               => 'Vous êtes ici :',
];

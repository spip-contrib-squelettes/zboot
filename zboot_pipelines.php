<?php
/**
 * Utilisations de pipelines par Zboot
 *
 * @plugin     Zboot
 * @copyright  2019
 * @author     Nicolas Dorigny
 * @licence    GNU/GPL
 * @package    SPIP\Zboot\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function zboot_pre_typo($texte) {
	// belles puces
	$texte = preg_replace('/^-\s?(?!\*|#|-)/m', '-* ', $texte);

	return $texte;
}

<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (substr(_request('recherche'), 0, 4) == 'http') {
	$ecran_securite_raison = 'Recherche mal formee';
	if ($GLOBALS['ip'] and date('s') == 0) {
		touch(_DIR_TMP . 'flood/' . $GLOBALS['ip']);
	}
}
if (isset($ecran_securite_raison)) {
	header("HTTP/1.0 403 Forbidden");
	header("Expires: Wed, 11 Jan 1984 05:00:00 GMT");
	header("Cache-Control: no-cache, must-revalidate");
	header("Pragma: no-cache");
	header("Content-Type: text/html");
	die("<html><title>Error 403: Forbidden</title><body><h1>Error 403</h1><p>You are not authorized to view this page ($ecran_securite_raison)</p></body></html>");
}

// ------------------------------------------------------------
// Définition de l'environnement : DEV / PREPROD / PROD
// ------------------------------------------------------------

if (isset($_SERVER["SERVER_ADDR"]) && $_SERVER["SERVER_ADDR"]) {
	if (in_array($_SERVER["SERVER_ADDR"], [
		"::1",
		"127.0.0.1",
	])) {
		define('_SERVEUR_MODE', 'DEV');
	} else {
		if ($_SERVER["SERVER_NAME"] == 'dev.domain.tld') {
			define('_SERVEUR_MODE', 'PREPROD');
		} else {
			define('_SERVEUR_MODE', 'PROD');
		}
	}
} else {
	// en mode CLI, on ne peut pas s'appuyer sur $_SERVER["SERVER_ADDR"], qui n'est pas défini
	// on teste sur le hostname
	if ($hostname = php_uname('n')) {
		switch ($hostname) {
			case "aaa" :
				// dév local
				define('_SERVEUR_MODE', 'DEV');
				break;

			case "bbb" :
				// préprod
				define('_SERVEUR_MODE', 'PREPROD');
				break;

			case "ccc" :
				// prod
				define('_SERVEUR_MODE', 'PROD');
				break;
		}
	}
}

// ------------------------------------------------------------
// Config selon l'environnement
// ------------------------------------------------------------

switch (_SERVEUR_MODE) {
	case 'DEV':
	case 'PREPROD':
		define('SPIP_ERREUR_REPORT', E_ALL); // & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT
		error_reporting(SPIP_ERREUR_REPORT);
		ini_set('display_errors', 1);
		define('_DEBUG_AUTORISER', true);
		define('_LOG_FILTRE_GRAVITE', 8);
		// ne pas envoyer du tout de mail
		define('_TEST_EMAIL_DEST', 'mon_adresse@domaine.tld');
		define('_SCSS_SOURCE_MAP', true);
		define('_SCSS_LINE_COMMENTS', true);
		break;
	case 'PROD':
		ini_set('display_errors', 0);
		define('_LOG_FILTRE_GRAVITE', 3);
		define('_SPIP_CHMOD', 0777);
		break;
}

// ------------------------------------------------------------
// 	Débug et logs
// ------------------------------------------------------------

define('_LOG_FILELINE', true);
define('_DEBUG_SLOW_QUERIES', true);
define('_BOUCLE_PROFILER', 1000);

// Ne pas remplacer < par &lt; dans les logs
define('_LOG_BRUT', true);

ini_set("log_errors", 1);
ini_set("error_log", $_SERVER['DOCUMENT_ROOT'] . '/tmp/log/php.log');

$nombre_de_logs = 6;
$taille_des_logs = 1024; // 1Mo avant rotation

// Adresses IP pour débug et var_mode=inclure à distance
$_DD_INFOS_IP = ['127.0.0.1', '::1', 'xx.xx.xx.xx'];

if (in_array($_SERVER['REMOTE_ADDR'], $_DD_INFOS_IP)) {
	ini_set('display_errors', 1);
	if (!defined('_DEBUG_AUTORISER')) {
		define('_DEBUG_AUTORISER', true);
	}
}

// ------------------------------------------------------------
//	Configs spécifiques
// ------------------------------------------------------------

// Pour que SPIP ne s'occupe pas de l'authentification Apache
$GLOBALS['ignore_auth_http'] = true;

// pas de portfolio
define('_BOUTON_MODE_IMAGE', false);

// plusieurs auteur-e-s ne peuvent avoir la même adresse mail.
define('_INTERDIRE_AUTEUR_MEME_EMAIL', true);

// crypter les noms des inputs dans les formulaires protégés par nospam
define('_SPAM_ENCRYPT_NAME', true);

// statut par défaut lors de la création d'un auteur
define('_STATUT_AUTEUR_CREATION', '6forum');

define('_SAISIES_AFFICHER_SI_JS_SHOW', 'slideDown(300)');
define('_SAISIES_AFFICHER_SI_JS_HIDE', 'slideUp(300)');

//--------------------------------------------------------------------
// Taille et poids maxi des images et logos

define('_IMG_MAX_WIDTH', 3000);
define('_IMG_MAX_HEIGHT', 3000);
define('_IMG_MAX_SIZE', 1024 * 4); # poids en ko

define('_LOGO_MAX_WIDTH', 3000);
define('_LOGO_MAX_HEIGHT', 3000);
define('_LOGO_MAX_SIZE', 1024 * 4); # poids en ko

define('_FILE_MAX_SIZE', 1024 * 12); # poids en ko

$(function() {
	globalPage.init();
	globalPage.initLayout();
	if(typeof onAjaxLoad == "function") {
		onAjaxLoad(globalPage.initLayout);
	}
});

var globalPage = (function() {
	var that = {};

	that.init = function() {

		$(window).resize(that.debounce(that.resize, 15));
		$(window).scroll(that.debounce(that.scroll, 15));

		const mediaQuery = window.matchMedia('(prefers-reduced-motion: reduce)');
		mediaQuery.addEventListener('change', () => {
			that.reduceMotion = mediaQuery.matches;
			that.fadeDuration = mediaQuery.matches ? 0 : 300;
		});
		mediaQuery.dispatchEvent(new Event('change'));

		// menu hamburger et Sidr
		$('.button--hamburger').sidr(
			{
				name: 'sidr-main',
				source: '.sidr-menu-content',
				renaming: false,
				onOpen: function() {$('.button--hamburger').addClass('is-active');},
				onClose: function() {$('.button--hamburger').removeClass('is-active');}
			}
		);
		$('#sidr-main a, .main_nav__secteur_mobile_close button').click(function() {
			$.sidr('close', 'sidr-main');
		});

		// navigation principale
		that.navMainMenu = $('.main_nav');
		that.navButtons = $('.main_nav__secteur_button');
		that.navMenus = $('.main_nav__secteur_menu');
		that.navBurger = $('button--hamburger');
		that.hideMenus = function(id) {
			that.navButtons.not('[aria-controls=' + id + ']').attr('aria-expanded', false);
			that.navMenus.not('#' + id).removeClass('main_nav__secteur_menu--open');
		};
		that.hideMobileNav = function() {
			that.navMainMenu.removeClass('main_nav__secteur_menu--open');
		};
		that.navButtons.on('click', function() {
			var id = $(this).attr('aria-controls');
			var menu = $('#' + id);
			that.hideMenus(id);
			$(this).attr('aria-expanded', ($(this).attr('aria-expanded') === 'false' ? 'true' : 'false'));
			menu.toggleClass('main_nav__secteur_menu--open');
		});
		that.navBurger.on('click', function(){
			$(this).attr('aria-expanded', ($(this).attr('aria-expanded') === 'false' ? 'true' : 'false'));
			that.navMainMenu.toggleClass('nav_main_menu--open');
		});
		$('.page_main').on('click', function(){that.hideMenus(null);});

		that.$footer = $('.page_footer');
		that.headerHeight = $('.page_nav').height()+$('.page_header').height();
		that.scrollableHeight = document.documentElement['scrollHeight'] - $(window).innerHeight() - that.$footer.height();
		that.windowScrollTop = $(window).scrollTop();
		
		// bouton retour haut de page
		$('.page_main').append('<a href="#top-page" id="link-top-page" class="back-to-top"><span class="visuallyhidden">Haut de la page</a>');
		that.mybutton = document.getElementById("link-top-page");
		that.mybutton.addEventListener('click',that.topFunction);
		console.log(that.mybutton);
		that.footer = document.getElementsByClassName('page_footer')[0];
		that.btnMarginBottom = parseInt(window.getComputedStyle(that.mybutton).getPropertyValue('bottom'));
		that.topCheck();
	}

	that.initLayout = function() {

		that.manageKeyboard();

		// Faire un lien sur un bloc entier
		$('.block-link-parent').each(function() {
			var down,
				up,
				link = $(this).find('a.block-link').first();
			if(!link.length) {
				link = $(this).find('a').first();
			}
			if(!link.length) {
				$(this).removeClass('block-link-parent');
			} else {
				$(this).off('mousedown').on('mousedown', function(e) {
					// si ce n'est pas un clic droit
					if(e.which !== 3) {
						down = +new Date();
					}
				});
				$(this).off('mouseup').on('mouseup', function(e) {
					if(e.which !== 3) {
						up = +new Date();
						if((up - down) < 200) {
							if(link.hasClass('spip_out') || e.which === 2 || e.ctrlKey || e.metaKey) {
								if(e.target.tagName.toLowerCase() !== 'a') {
									window.open(link.attr('href'));
								}
							} else {
								window.location.href = link.attr('href');
							}
						}
					}
				});
			}
		});

		// liens sortant et PDF en target _blank + ajout d'une mention dans le title ou alt des images
		$('a.spip_out, a.spip_url, a.spip_glossaire, a.external, a[href$=".pdf"], a[href$=".doc"]')
			.each(function() {
				var suffix = ' - Nouvelle fenêtre';
				var clone = $(this).clone();
				clone.find('[aria-hidden="true"]').remove();
				var text = clone.text().trim();
				// TODO : supprimer $('[aria-hidden=true]') du texte
				$(this).attr('target', '_blank').attr('rel', 'noreferrer noopener');
				var attribute = $(this).attr('aria-label') ? 'aria-label' : 'title';
				if($(this).attr(attribute) && $(this).attr(attribute).length) {
					if(!$(this).attr(attribute).indexOf(suffix)) {
						$(this).attr(attribute, $(this).attr(attribute) + suffix);
					}
				} else if(text.length) {
					$(this).attr(attribute, text + suffix);
				} else {
					$(this).children('img').each(function() {
						attribute = $(this).attr('aria-label') ? 'aria-label' : 'alt';
						$(this).attr(attribute, $(this).attr(attribute) + suffix);
					});
				}
			});

	};

	that.debounce = function(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this,
				args = arguments;
			var later = function() {
				timeout = null;
				if(!immediate) {
					func.apply(context, args);
				}
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait || 200);
			if(callNow) {
				func.apply(context, args);
			}
		};
	};

	that.resize = function() {
		$(window).scroll();
	};

	that.scroll = function() {
		that.topCheck();
	};

	that.topCheck = function() {
		// afficher / masquer le lien back-top
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			that.mybutton.classList.add('back-to-top--visible');
		} else {
			that.mybutton.classList.remove('back-to-top--visible');
		}
		// le positionner au dessus du footer
		const footStartPos = that.footer.getBoundingClientRect().y;
		if (window.innerHeight > footStartPos) {
			that.mybutton.style.bottom = `${(window.innerHeight - footStartPos) + that.btnMarginBottom}px`;
		} else {
			that.mybutton.style.bottom = '';
		}
	}
	that.topFunction = function() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
	
	that.manageKeyboard = function() {
		$('body').on('keyup', function(e) {
			var keyCode = e.which || e.keyCode;
			var classList = e.target.classList;
			// Tab
			if(keyCode === 9 && !e.shiftKey) {
				// Si on arrive sur un bouton de la nav principale, on ferme le menu en cours
				if(classList.contains('main_nav__secteur_button') && e.target.attributes['aria-controls'].value) {
					that.hideMenus(e.target.attributes['aria-controls'].value);
				} else if(!classList.contains('main_nav__secteur_button') && !classList.contains('main_nav__secteur_menu_link')) {
					if(
						!that.navMainMenu.hasClass('nav_main_menu--open')
						//|| (!classList.contains('nav_search__button') && !classList.contains('nav_contact__button'))
					) {
						that.hideMenus();
						that.hideMobileNav();
					}
				}
			}
			// Shift + Tab
			if(keyCode === 9 && e.shiftKey) {
				// s'il y a un menu ouvert, on le ferme
				if(classList.contains('main_nav__secteur_button') && e.target.attributes['aria-expanded'].value) {
					that.hideMenus();
				} else if(!classList.contains('main_nav__secteur_button') && !classList.contains('main_nav__secteur_menu_link')) {
					that.hideMenus();
					that.hideMobileNav();
				}
			}
			// Esc
			if(keyCode === 27) {
				that.hideMenus();
				that.hideMobileNav();
			}
		});
	};

	return that;

})();

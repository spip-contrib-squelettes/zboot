#!/bin/bash

# Ce script renomme le plugin avec un nouveau préfixe
 
if [[ $# -eq 0 ]] ; then
  echo 'Erreur : indiquer le nouveau préfixe en paramètre'
  exit 1
fi

prefixe=$1

read -r -p "Renommer le préfixe zboot en ${prefixe} ?  [O/n]" response
if [[ $response =~ ^(O|o| ) ]] || [[ -z $response ]]; then
  # renommer les fichiers avec le nouveau préfixe 
  find . -type f -name '*zboot*' | sed "p;s/zboot/${prefixe}/" | xargs -n2 mv
  # différence de syntaxe de sed entre MacOS / BSD et GNU/Linux 
  if [[ "$OSTYPE" == "darwin"* ]]; then
    # rechercher/remplacer zboot par le nouveau préfixe dans certains fichiers
    find . -type f \( -name "*.php" -o -name "*.html" -o -name "*.xml" \) -print0 | xargs -0 sed -i '' "s/zboot/$prefixe/g"
    # rechercher/remplacer Zboot par le nouveau préfixe en ucfirst
    ucfirst="$(tr '[:lower:]' '[:upper:]' <<<${prefixe:0:1})${prefixe:1}"
    find . -type f \( -name "*.php" -o -name "*.html" -o -name "*.xml" \) -print0 | xargs -0 sed -i '' "s/Zboot/$ucfirst/g"
  else
    # rechercher/remplacer zboot par le nouveau préfixe dans certains fichiers
    find . -type f \( -name "*.php" -o -name "*.html" -o -name "*.xml" \) -print0 | xargs -0 sed -i "s/zboot/$prefixe/g"
    # rechercher/remplacer Zboot par le nouveau préfixe en ucfirst
    ucfirst="$(tr '[:lower:]' '[:upper:]' <<<${prefixe:0:1})${prefixe:1}"
    find . -type f \( -name "*.php" -o -name "*.html" -o -name "*.xml" \) -print0 | xargs -0 sed -i "s/Zboot/$ucfirst/g"
  fi
  # terminé
  echo 'Préfixe renommé.'
fi

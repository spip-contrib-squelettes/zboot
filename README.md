# Zboot

Squelette de démarrage rapide pour SPIP avec Zcore et ScssPhp

Ce plugin n'est pas à installer tel quel, mais à copier comme base de travail puis à modifier.
Il n'a donc pas de tag ni de release.

## Utilisation

1/ Cloner ce dépôt

```
cd /var/www/chemin_du_site/plugins
git clone https://github.com/nd-/zboot monprojet
```

2/ Lancer le script `rename.sh` en lui passant le nouveau préfixe à utiliser, pour pouvoir travailler sur un nouveau projet avec son propre préfixe

```
cd monprojet
chmod +x rename.sh
./rename.sh monprojet
rm rename.sh
```

 _(le script renomme les fichiers zboot* par monprojet*)_

3/ Remplacer le logo `/prive/themes/spip/images/zboot-xx.svg` par celui du projet, et retirer le crédit dans `paquet.xml`

4/ Modifier son dépôt source pour le "débrancher" de ce dépôt et le versionner sur son propre dépôt

```
git remote set-url origin git://new.url.here
```

## Fichiers de configuration

Les deux fichiers `mes_options.php` et `connect.php` permettent de gérer la configuration et l'accès aux bases de données selon l'environnement (dév, préprod, prod).

Le fichier `connect.php` est à déplacer dans /config en reprenant éventuellement les paramètres existant.

Le fichier `mes_options.php` est à installer avec un lien symbolique dans /config, afin qu'il soit toujours à jour par rapport à celui qui est versionné.

```
mv /var/www/chemin_du_site/plugins/monprojet/connect.php /var/www/chemin_du_site/config/
ln -s /var/www/chemin_du_site/plugins/monprojet/mes_options.php /var/www/chemin_du_site/config/
```

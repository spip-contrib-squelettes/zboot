<?php
/**
 * Fonctions utiles au plugin Zboot
 *
 * @plugin     Zboot
 * @copyright  2019
 * @author     Nicolas Dorigny
 * @licence    GNU/GPL
 * @package    SPIP\Zboot\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if(!function_exists('filtre_pagination_affiche_texte_lien_page')) {
	function filtre_pagination_affiche_texte_lien_page($type_pagination, $numero_page, $rang_item) {
		if ($numero_page === 'tous') {
			return '&#8734;';
		}
		if ($numero_page === 'prev') {
			return _T('zboot:pagination_prec');
		}
		if ($numero_page === 'next') {
			return _T('zboot:pagination_suiv');
		}

		switch ($type_pagination) {
			case 'resultats':
				return $rang_item + 1; // 1 11 21 31...
			case 'naturel':
				return $rang_item ?: 1; // 1 10 20 30...
			case 'rang':
				return $rang_item; // 0 10 20 30...

			case 'page':
			case 'prive':
			default:
				return $numero_page; // 1 2 3 4 5...
		}
	}
}
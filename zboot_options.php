<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

define('_PAGINATION_NOMBRE_LIENS_MAX', 6);

// corriger les intertitres de SPIP
$GLOBALS['debut_intertitre'] = "\n<h2>";
$GLOBALS['fin_intertitre'] = "</h2>\n";

// Blocs Zcore
$GLOBALS['z_blocs'] = [
	'content',
	'head',
	'header',
	'breadcrumb',
	'aside',
	'footer',
];

define('_LONGUEUR_TITRES_BREADCRUMB', 30);

define('SOMMAIRE_GENERER_SECTIONS', false);

define('_CONTENT_WIDTH', 730);
define('_IMAGE_TAILLE_MINI_AUTOLIEN', _CONTENT_WIDTH * 1.15);
define('_LOGO_WIDTH', 240);
define('_IMAGE_RATIO', '1.5:1');

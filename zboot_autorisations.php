<?php
/**
 * Définit les autorisations du plugin Zboot
 *
 * @plugin     Zboot
 * @copyright  2019
 * @author     Nicolas Dorigny
 * @licence    GNU/GPL
 * @package    SPIP\Zboot\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline
 *
 * @pipeline autoriser
 */
function zboot_autoriser() {
}

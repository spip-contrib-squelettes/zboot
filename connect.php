<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

// Configuration de l'accès à la base de données MySql en fonction de l'environnement (défini dans mes_options.php)

defined('_MYSQL_SET_SQL_MODE') || define('_MYSQL_SET_SQL_MODE', true);
$GLOBALS['spip_connect_version'] = 0.8;

switch (_SERVEUR_MODE) {
	case 'DEV':
		spip_connect_db('localhost', '', 'root', '****', 'monprojet', 'mysql', 'spip', '', 'utf8');
		break;
	case 'PREPROD':
		spip_connect_db('localhost', '', 'user_dev', 'pass', 'database_dev', 'mysql', 'spip', '', 'utf8');
		break;
	case 'PROD':
		spip_connect_db('localhost', '', 'user_prod', 'pass', 'database_prod', 'mysql', 'spip', '', 'utf8');
		break;
}


